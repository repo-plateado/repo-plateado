package com.plateado.pregol.ghuidobro.pregol;

/**
 * Created by ghuidobro on 6/6/17.
 */

class MGCredito {
    private String usuario;
    private int saldo;
    private int depositoUsuario;
    private int depositoApp;

    public MGCredito(String usuario, int deposito_usuario, int deposito_app, int saldo) {
        this.usuario = usuario;
        this.depositoUsuario = deposito_usuario;
        this.depositoApp = deposito_app;
        this.saldo = saldo;

    }



    public int getDepositoUsuario() {
        return depositoUsuario;
    }

    public void setDepositoUsuario(int depositoUsuario) {
        this.depositoUsuario = depositoUsuario;
    }

    public int getDepositoApp() {
        return depositoApp;
    }

    public void setDepositoApp(int depositoApp) {
        this.depositoApp = depositoApp;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
}
