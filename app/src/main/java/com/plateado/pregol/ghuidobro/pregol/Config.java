package com.plateado.pregol.ghuidobro.pregol;

/**
 * Created by Belal on 10/24/2015.
 */
public class Config {

    //Address of our scripts of the CRUD
    public static final String URL_ADD="https://monygol2021.000webhostapp.com/monygol/mobile/addprediccion.php";
    public static final String URL_PAGO_CREDITO = "https://monygol2021.000webhostapp.com/monygol/mobile/pagocredito.php";
    public static final String URL_REGISTRO_PAGO_USUARIO_CONSULTA = "https://monygol2021.000webhostapp.com/monygol/mobile/consultapagoinscripcionusuario.php?usuario=";
    public static final String URL_PAGO_CREDITO_USUARIO = "https://monygol2021.000webhostapp.com/monygol/mobile/consultapagocreditousuario.php?usuario=";
    public static final String URL_REGISTRO_PAGO_USUARIO = "https://monygol2021.000webhostapp.com/monygol/mobile/registropagousuario.php";
    public static final String URL_BORRADO_INSCRIPCION_PREDICCION_USUARIO = "https://monygol2021.000webhostapp.com/monygol/mobile/borrarprediccionususario.php?usuario=";
    public static final String URL_FIXTURE = "https://monygol2021.000webhostapp.com/monygol/mobile/fixture.php";
    public static final String URL_PREDICCION_USUARIO = "https://monygol2021.000webhostapp.com/monygol/mobile/prediccionusuario.php?usuario=";
    public static final String URL_RESULTADO_FECHA = "https://monygol2021.000webhostapp.com/monygol/mobile/consultaresultado.php";
    public static final String URL_CONSULTA_VIDA_FECHA = "https://monygol2021.000webhostapp.com/monygol/mobile/consultavidafecha.php";
    public static final String URL_POSICION_USUARIO = "https://monygol2021.000webhostapp.com/monygol/mobile/consultaposicionusuario.php?usuario=";
    public static final String URL_CONSULTA_MENSAJE_FOOTER = "https://monygol2021.000webhostapp.com/monygol/mobile/consultamesaggefooter.php";


    public static final String KEY_INSCRIPCION_FECHA = "10";

    //Configuracion cantidad de partidos por fecha
    public static final Integer PARTIDOSXFECHA = 3;

}
