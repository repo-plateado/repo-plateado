package com.plateado.pregol.ghuidobro.pregol;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.util.Attributes;
import com.github.ybq.android.spinkit.style.DoubleBounce;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ghuidobro on 5/22/17.
 */

public class EstadisticasFragment extends Fragment implements View.OnClickListener{

    private SessionManager session;
    private String usuario;
    private AlertDialogManager alert = new AlertDialogManager();
    private ProgressBar progressBar;
    private ArrayList<ItemResultado> mglistResultadoFecha = new ArrayList<>();
    private ArrayList<ItemResultado> mglistPosicionFecha = new ArrayList<>();
    List<MGMessageFooter> mglistMessageFooter = new ArrayList<>();
    private ResultadoFechaViewAdapter resultadoFechaViewAdapter;
    private RecyclerView mRecyclerView;
    private TextView tvLabelSinResultado,tvPuntosUsuario,tvMensajeFooter,tvFechaFixture;
    RelativeLayout footer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View fragmentView = inflater.inflate(R.layout.estadisticas_view, container, false);

        tvLabelSinResultado = (TextView) fragmentView.findViewById(R.id.tv_label_sin_resultado);
        tvPuntosUsuario = (TextView) fragmentView.findViewById(R.id.tv_puntos_usuario);
        tvFechaFixture = (TextView) fragmentView.findViewById(R.id.tv_fecha_fixture);

        resultadoFechaViewAdapter = new ResultadoFechaViewAdapter(getActivity(),mglistResultadoFecha);
        resultadoFechaViewAdapter.setMode(Attributes.Mode.Single);

        mRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.my_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(resultadoFechaViewAdapter);

        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null,false,true));

        progressBar = (ProgressBar)fragmentView.findViewById(R.id.spin_kit);
        DoubleBounce doubleBounce = new DoubleBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);

        session = new SessionManager(getActivity());
        if(session.getUserDetails() != null){
            usuario = session.getUserDetails().get("email");
        }
        if(Utils.isOnline(getActivity())) {
            loadResultadoRequest();
            loadResultadoRequestUsuario(usuario);
        }else{
            alert.showAlertDialog(getActivity(),"Sin Coneccion","Asegurate de tener coneccion a internet.",false);
        }

        footer = (RelativeLayout) fragmentView.findViewById(R.id.footer);

        tvMensajeFooter = (TextView) fragmentView.findViewById(R.id.tv_mensajes_footer);
        tvMensajeFooter.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tvMensajeFooter.setSelected(true);
        tvMensajeFooter.setSingleLine(true);

        loadMensajeFooter();

        return fragmentView;
    }

    private void loadResultadoRequest() {
        // load initial data
        try{
            class CreditosTask extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        convertPagoCreditoJSONtoArrayList(jsonObj);
                        if(!mglistResultadoFecha.isEmpty()){
                            mRecyclerView.setVisibility(View.VISIBLE);
                            resultadoFechaViewAdapter.notifyDataSetChanged();
                        }else{
                            tvLabelSinResultado.setVisibility(View.VISIBLE);
                        }

                        progressBar.setVisibility(View.GONE);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendGetRequest(Config.URL_RESULTADO_FECHA);
                    return res;
                }
            }
            CreditosTask creditosTask = new CreditosTask();
            creditosTask.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    //metodo para consultar posicion usuario
    private void loadResultadoRequestUsuario(final String usuario) {
        // load initial data
        try{
            class CreditosTask extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        convertposicionUsuarioJSONtoArrayList(jsonObj);
                        if(!mglistPosicionFecha.isEmpty()){
                            for (ItemResultado item :mglistPosicionFecha) {
                                tvPuntosUsuario.setText(item.getPuntos());
                                tvFechaFixture.setText(item.getFechaFixture());

                            }
                        }else{
                            tvPuntosUsuario.setText("0");
                            tvFechaFixture.setText("0");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendGetRequestParam(Config.URL_POSICION_USUARIO, usuario);
                    return res;
                }
            }
            CreditosTask creditosTask = new CreditosTask();
            creditosTask.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onClick(View v) {

        }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void convertPagoCreditoJSONtoArrayList(JSONObject jsonObj) {

        try{
            JSONArray list = jsonObj.getJSONArray("resultadoFecha");
            String posicionResultadoFecha = "";
            for(int i=0; i<list.length(); i++) {
                JSONObject stateobj = list.getJSONObject(i);

                switch (i){
                    case(0):
                            posicionResultadoFecha = "1°";
                        break;
                    case(1):
                            posicionResultadoFecha = "2°";
                        break;
                    case(2):
                            posicionResultadoFecha = "3°";
                        break;
                }
                mglistResultadoFecha.add(new ItemResultado( posicionResultadoFecha,stateobj.getString("usuario"),stateobj.getString("puntaje"),stateobj.getString("fecha_fixture")));

            }//end of for loop

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }

    private void convertposicionUsuarioJSONtoArrayList(JSONObject jsonObj) {

        try{
            JSONArray list = jsonObj.getJSONArray("consultaposicionusuario");
            String posicionResultadoFecha = "";
            for(int i=0; i<list.length(); i++) {
                JSONObject stateobj = list.getJSONObject(i);
                mglistPosicionFecha.add(new ItemResultado( posicionResultadoFecha,stateobj.getString("usuario"),stateobj.getString("puntaje"),stateobj.getString("fecha_fixture")));

            }//end of for loop

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }

    private void loadMensajeFooter() {
        // load initial data
        try{
            class MessageTask extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        convertMensajeFooterJSONtoArrayList(jsonObj);
                        if(!mglistMessageFooter.isEmpty()){
                            for (MGMessageFooter item: mglistMessageFooter){
                                if(item.getMessageFooter() != null && !item.getMessageFooter().isEmpty()){
                                    tvMensajeFooter.setText(item.getMessageFooter());
                                }else{
                                    tvMensajeFooter.setText("Sigue los resultados en nuestras redes sociales y comparte con tus amigos Monygol");
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendGetRequest(Config.URL_CONSULTA_MENSAJE_FOOTER);
                    return res;
                }
            }
            MessageTask messageTask = new MessageTask();
            messageTask.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void convertMensajeFooterJSONtoArrayList(JSONObject jsonObj) {

        try{
            JSONArray list = jsonObj.getJSONArray("mensajefooter");

            for(int i=0; i<list.length(); i++) {
                JSONObject stateobj = list.getJSONObject(i);
                mglistMessageFooter.add(new MGMessageFooter(stateobj.getString("mensaje_footer")));
            }//end of for loop

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }

    public class MGMessageFooter{


        public MGMessageFooter(String messageFooter) {
            this.messageFooter = messageFooter;
        }

        public String getMessageFooter() {
            return messageFooter;
        }

        public void setMessageFooter(String messageFooter) {
            this.messageFooter = messageFooter;
        }

        String messageFooter;

    }


}