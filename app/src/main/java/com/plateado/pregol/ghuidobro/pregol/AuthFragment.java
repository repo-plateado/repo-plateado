package com.plateado.pregol.ghuidobro.pregol;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ghuidobro on 5/22/17.
 */

public class AuthFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener{

    private GoogleApiClient mGoogleApiClient;
    private SignInButton btnSignIn;
    private Button btnSignOut,btnFixture;
    private TextView txtName, txtEmail,welcomeLabel,tvMensajeFooter;
    private static final int RC_SIGN_IN = 007;
    private Utils util = new Utils();
    private AlertDialogManager alert = new AlertDialogManager();
    List<MGPagoInscripcion> mglistRegistroInscripcionUsuario;
    List<MGMessageFooter> mglistMessageFooter;
    String registroPago,fechaFixture;
    private LinearLayout userNameLL;
    private ImageView iconApp,appLogo;
    RelativeLayout footer;

    // Session Manager Class
    private SessionManager session;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Session Manager
        session = new SessionManager(getContext());
        mglistRegistroInscripcionUsuario = new ArrayList<MGPagoInscripcion>();
        mglistMessageFooter = new ArrayList<MGMessageFooter>();
        registroPago = "";
        fechaFixture = "";

        final View fragmentView = inflater.inflate(R.layout.auth_view, container, false);

        // Set the dimensions of the sign-in button.
        SignInButton signInButton = (SignInButton) fragmentView.findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);

        btnSignIn = (SignInButton) fragmentView.findViewById(R.id.sign_in_button);
        btnSignOut = (Button) fragmentView.findViewById(R.id.btn_sign_out);
        btnFixture = (Button) fragmentView.findViewById(R.id.btn_fixture);
        txtName = (TextView) fragmentView.findViewById(R.id.txtName);
        txtEmail = (TextView) fragmentView.findViewById(R.id.txtEmail);
        welcomeLabel = (TextView) fragmentView.findViewById(R.id.welcome_label);
        iconApp = (ImageView) fragmentView.findViewById(R.id.iconApp);
        appLogo = (ImageView) fragmentView.findViewById(R.id.appLogo);

        footer = (RelativeLayout) fragmentView.findViewById(R.id.footer);

        tvMensajeFooter = (TextView) fragmentView.findViewById(R.id.tv_mensajes_footer);
        tvMensajeFooter.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tvMensajeFooter.setSelected(true);
        tvMensajeFooter.setSingleLine(true);

        welcomeLabel.setText(R.string.welcome_label);

        userNameLL = (LinearLayout) fragmentView.findViewById(R.id.name_user_LL);

        btnSignIn.setOnClickListener(this);
        btnSignOut.setOnClickListener(this);
        btnFixture.setOnClickListener(this);


        // Configure sign-in to request the user's ID, email address, and basic
// profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
// options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .enableAutoManage((FragmentActivity) getContext() /* FragmentActivitSy */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        loadMensajeFooter();

        return fragmentView;
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.sign_in_button:
                signIn();
                break;

            case R.id.btn_sign_out:
                signOut();
                break;

            case R.id.btn_fixture:
                goFixture();
                break;
        }
    }

    private void goFixture() {

        MGMainFixtureFragment fragment = new MGMainFixtureFragment();
        if (fragment != null) {
            getActivity().getSupportFragmentManager().beginTransaction()
            .replace(R.id.content_frame, fragment)
            .commit();
        }
    }

    private void signIn() {
        if(Utils.isOnline(getActivity())) {
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }else{
            alert.showAlertDialog(getActivity(),"Sin Coneccion","Asegurate de tener coneccion a internet.",false);
        }

    }
    private void signOut() {
        if(Utils.isOnline(getActivity())) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            session.logoutUser();
                            updateUI(false);
                        }
                    });
        }else{
            alert.showAlertDialog(getActivity(),"Sin Coneccion","Asegurate de tener coneccion a internet.",false);
        }

    }

    private void revokeAccess() {
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        updateUI(false);
                    }
                });
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String personName = acct.getDisplayName();
            String email = acct.getEmail();

            session.createLoginSession("MonyGol", email);

            txtName.setText(personName);
            txtEmail.setText(email);

            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            updateUI(false);
        }
    }

    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            btnSignIn.setVisibility(View.GONE);
            iconApp.setVisibility(View.GONE);
            appLogo.setVisibility(View.GONE);
            btnSignOut.setVisibility(View.VISIBLE);
            txtName.setVisibility(View.VISIBLE);
            txtEmail.setVisibility(View.VISIBLE);
            btnFixture.setVisibility(View.VISIBLE);
            userNameLL.setVisibility(View.VISIBLE);
            welcomeLabel.setVisibility(View.GONE);
        } else {
            btnSignIn.setVisibility(View.VISIBLE);
            iconApp.setVisibility(View.VISIBLE);
            appLogo.setVisibility(View.VISIBLE);
            btnSignOut.setVisibility(View.GONE);
            txtName.setVisibility(View.GONE);
            txtEmail.setVisibility(View.GONE);
            btnFixture.setVisibility(View.GONE);
            userNameLL.setVisibility(View.GONE);
            welcomeLabel.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage((FragmentActivity) getContext());
        mGoogleApiClient.disconnect();
    }

    private void loadMensajeFooter() {
        // load initial data
        try{
            class MessageTask extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        convertMensajeFooterJSONtoArrayList(jsonObj);
                        if(!mglistMessageFooter.isEmpty()){
                            for (MGMessageFooter item: mglistMessageFooter){
                                if(item.getMessageFooter() != null && !item.getMessageFooter().isEmpty()){
                                    tvMensajeFooter.setText(item.getMessageFooter());
                                }else{
                                    tvMensajeFooter.setText("Sigue los resultados en nuestras redes sociales y comparte con tus amigos Monygol");
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendGetRequest(Config.URL_CONSULTA_MENSAJE_FOOTER);
                    return res;
                }
            }
            MessageTask messageTask = new MessageTask();
            messageTask.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void convertMensajeFooterJSONtoArrayList(JSONObject jsonObj) {

        try{
            JSONArray list = jsonObj.getJSONArray("mensajefooter");

            for(int i=0; i<list.length(); i++) {
                JSONObject stateobj = list.getJSONObject(i);
                mglistMessageFooter.add(new MGMessageFooter(stateobj.getString("mensaje_footer")));
            }//end of for loop

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }

    public class MGMessageFooter{


        public MGMessageFooter(String messageFooter) {
            this.messageFooter = messageFooter;
        }

        public String getMessageFooter() {
            return messageFooter;
        }

        public void setMessageFooter(String messageFooter) {
            this.messageFooter = messageFooter;
        }

        String messageFooter;

    }

}