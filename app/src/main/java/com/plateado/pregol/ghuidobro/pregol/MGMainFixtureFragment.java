package com.plateado.pregol.ghuidobro.pregol;

import android.app.DialogFragment;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.util.Attributes;
import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ghuidobro on 4/12/17.
 */
public class MGMainFixtureFragment extends Fragment {

    private ArrayList<MGFixture> mDataSet;
    private TextView emptyView;
    private RecyclerView mRecyclerView;
    private ImageView iconApp,appLogo;
    private FloatingActionButton fab;
    HashMap<String, String> prediccion ;
    SwipeRecyclerViewAdapter mAdapter;
    private String sendMessage = "La prediccion no esta completa !";
    private boolean isOKToSend = false;
    ArrayList<HashMap> aList;
    String usuario;
    ArrayList<MGItemPrediction> MGItemPredictions = new ArrayList<>();
    private FABToolbarLayout morph;
    private Handler handler;
    private TextView fechaFixture;
    View info, borrarJugada, sendIcon, botonMainAction;
    private SessionManager session;
    private Utils util = new Utils();
    private AlertDialogManager alert = new AlertDialogManager();
    private String localFechaFixture;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                Bundle savedInstanceStat) {
        final View fragmentView = inflater.inflate(R.layout.swiprecyclerview, container, false);

        emptyView = (TextView) fragmentView.findViewById(R.id.empty_view);
        iconApp = (ImageView) fragmentView.findViewById(R.id.iconApp);
        appLogo = (ImageView) fragmentView.findViewById(R.id.app_logo);
        mRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.my_recycler_view);
        fab = (FloatingActionButton) fragmentView.findViewById(R.id.fab);
        morph = (FABToolbarLayout) fragmentView.findViewById(R.id.fabtoolbar);
        sendIcon = fragmentView.findViewById(R.id.send_icon);
        fechaFixture = (TextView) fragmentView.findViewById(R.id.fecha_fixture);
        info = fragmentView.findViewById(R.id.uno);
        borrarJugada = fragmentView.findViewById(R.id.borrar_jugada);
        botonMainAction = fragmentView.findViewById(R.id.btn_main_action);
        session = new SessionManager(getActivity());

        if(!session.getUserDetails().get("email").isEmpty()){
            usuario = session.getUserDetails().get("email");
        }

        handler = new Handler();

        // Layout Managers:
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.fab) {
                    morph.show();
                }
                morph.hide();
            }
        });
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callInfodialog();
                morph.hide();
            }
        });
        borrarJugada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                borrarPrediccion();
                morph.hide();
            }
        });
        sendIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendData();
                String colorSnackbar = "";
                if(isOKToSend){
                    sendMessage = "Se envio con exito!";
                    colorSnackbar = "#008b08";
                }else{
                    colorSnackbar = "#a92b00";
                }
                morph.hide();

                Snackbar snack = Snackbar.make(view, sendMessage, Snackbar.LENGTH_LONG);
                View snackbarView = snack.getView();
                snackbarView.setBackgroundColor(Color.parseColor(colorSnackbar.toString()));
                snackbarView.setMinimumHeight(210);
                TextView tv = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tv.setTextColor(ContextCompat.getColor(getActivity(), R.color.window_background));
                tv.setTextSize(20);
                tv.setGravity(Gravity.CENTER_HORIZONTAL);

                snack.show();
            }
        });
        botonMainAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                morph.hide();
            }
        });


        mDataSet = new ArrayList<MGFixture>();

        if(Utils.isOnline(getActivity())) {
            loadPrediccionData();
            loadData();
        }else{
            alert.showAlertDialog(getActivity(),"Sin Coneccion","Asegurate de tener coneccion a internet.",false);
        }

        prediccion = new HashMap<>();
        aList = new ArrayList<>();
        // Creating Adapter object
        mAdapter = new SwipeRecyclerViewAdapter(getActivity(), MGItemPredictions,prediccion,aList);

        mAdapter.setMode(Attributes.Mode.Single);

        mRecyclerView.setAdapter(mAdapter);

        /* Scroll Listeners */
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.i("RecyclerView", "onScrollStateChanged");
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        return fragmentView;

    }

    private void borrarPrediccion() {
        MGMainFixtureFragment fragment = new MGMainFixtureFragment();
        if (fragment != null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();
        }

    }

    private void sendData(){

        if(mAdapter.prediccion.size() == Config.PARTIDOSXFECHA){
            prediccion = (HashMap<String, String>) mAdapter.prediccion;
            prediccion.put("fecha",localFechaFixture);
            prediccion.put("usuario",usuario);
            isOKToSend = true;

            class AddFixture extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    new Thread(new Task()).start();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    MGPrediccionesFragment fragment = new MGPrediccionesFragment();
                    if (fragment != null) {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.content_frame, fragment)
                                .commit();
                    }
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendPostRequest(Config.URL_ADD, prediccion);
                    return res;
                }
            }

            AddFixture addFixture = new AddFixture();
            addFixture.execute();

        }else{
            isOKToSend = false;
        }

    }


    public void callInfodialog() {
        DialogFragment newFragment = new MGDialogClass(R.layout.info_custom_dialog,true);
        newFragment.show(getActivity().getFragmentManager(),"");
    }

    public void onResume(){
        super.onResume();

        if (mDataSet.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            morph.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            iconApp.setVisibility(View.VISIBLE);
            appLogo.setVisibility(View.VISIBLE);

        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            morph.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            iconApp.setVisibility(View.GONE);
            appLogo.setVisibility(View.GONE);

        }
    }

    // load initial data
    public void loadData() {
        String urlString = Config.URL_FIXTURE;

        try{
            URL url = new URL(urlString);
            StateTask stateTask = new StateTask();
            stateTask.execute(url);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private class StateTask extends AsyncTask<URL, String,JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {

            HttpURLConnection connection = null;
            try{

                connection = (HttpURLConnection)params[0].openConnection();
                int response = connection.getResponseCode();

                if(response == HttpURLConnection.HTTP_OK){

                    StringBuilder builder = new StringBuilder();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String line;
                        while ((line = reader.readLine())!= null ) {
                            builder.append(line);
                        }
                    }catch (IOException e){
                        publishProgress("Read Error");
                        e.printStackTrace();
                    }//inner try

                    return new JSONObject(builder.toString());

                }else{
                    publishProgress("Connection Error");
                }


            }//end outer try
            catch (Exception e){

                publishProgress("Connection Error 2");
                e.printStackTrace();

            }finally {
                connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            //Toast.makeText(MainActivity.this, values[0], Toast.LENGTH_SHORT).show();
        }//end of onProgressUpdate

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            convertFixtureJSONtoArrayList(jsonObject);

            for (MGFixture item:mDataSet) {
                MGItemPrediction MGItemPrediction = new MGItemPrediction();
                MGItemPrediction.setMGFixture(item);
                MGItemPrediction.setEstado(MGItemPrediction.SIN_ESTADO);
                MGItemPredictions.add(MGItemPrediction);
            }

            mAdapter.notifyDataSetChanged();

        }//end of onPostExecute
    }//end of



    private class Task implements Runnable {
        @Override
        public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        //loading = ProgressDialog.show(getActivity(), "Enviando...", "Un momento...", false, false);
                    }
                });
        }
    }

    private void convertFixtureJSONtoArrayList(JSONObject states){

        mDataSet.clear();

        try{
            JSONArray list = states.getJSONArray("fixture");

            for(int i=0; i<list.length(); i++) {
                JSONObject stateobj = list.getJSONObject(i);
                mDataSet.add(new MGFixture(stateobj.getString("equipo_local"),stateobj.getString("equipo_visitante"),stateobj.getString("escudo_local"),stateobj.getString("escudo_visita"),stateobj.getString("id_partido"),stateobj.getString("numero_fecha_afa")));
            }//end of for loop
            if (mDataSet.isEmpty()) {
                mRecyclerView.setVisibility(View.GONE);
                morph.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
                iconApp.setVisibility(View.VISIBLE);
                appLogo.setVisibility(View.VISIBLE);

            } else {
                mRecyclerView.setVisibility(View.VISIBLE);
                morph.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
                iconApp.setVisibility(View.GONE);
                appLogo.setVisibility(View.GONE);
                localFechaFixture = mDataSet.get(0).getFechaFixture();
                fechaFixture.setText("Fecha Fixture " + mDataSet.get(0).getFechaFixture());
            }

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }//end of convertJSONArrayList


    // load initial data
    public void loadPrediccionData() {
        final String urlString = Config.URL_PREDICCION_USUARIO;
        try{
            class AddFixture extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        convertPredictionJSONtoArrayList(jsonObj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendGetRequestParam(urlString, usuario);
                    return res;
                }
            }
            AddFixture addFixture = new AddFixture();
            addFixture.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void convertPredictionJSONtoArrayList(JSONObject states){
        try{
            JSONArray list = states.getJSONArray("prediccionusuario");
            if(list.length() == 0) {
                    //alert.showAlertDialog(getActivity(),"No hay Prediccion","Por el momento no has hecho una prediccion elegi y empeza a jugar.",true);
            }else{
                //alert.showAlertDialog(getActivity(),"Hay Prediccion hecha","La prediccion esta hecha y es una muy buena jugada, si estas inscripto esperemos que ganes.",true);
                MGPrediccionesFragment fragment = new MGPrediccionesFragment();
                if (fragment != null) {
                    getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment)
                    .commit();
                }

            }

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }//end of convertJSONArrayList

}
