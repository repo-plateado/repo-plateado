package com.plateado.pregol.ghuidobro.pregol;

/**
 * Created by ghuidobro on 6/6/17.
 */

class MGPagoInscripcion {
    private String usuario;
    private String fechaFixture;
    private String registroPago;

    public MGPagoInscripcion(String usuario, String fechaFixture, String registroPago) {
        this.usuario = usuario;
        this.fechaFixture = fechaFixture;
        this.registroPago = registroPago;

    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    public String getRegistroPago() {
        return registroPago;
    }

    public void setRegistroPago(String registroPago) {
        this.registroPago = registroPago;
    }

    public String getFechaFixture() {
        return fechaFixture;
    }

    public void setFechaFixture(String fechaFixture) {
        this.fechaFixture = fechaFixture;
    }

}
