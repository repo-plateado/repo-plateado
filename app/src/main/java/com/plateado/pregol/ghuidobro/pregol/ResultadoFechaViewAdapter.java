package com.plateado.pregol.ghuidobro.pregol;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.ArrayList;

public class ResultadoFechaViewAdapter extends RecyclerSwipeAdapter<ResultadoFechaViewAdapter.SimpleViewHolder> {


    private Context mContext;
    private ArrayList<ItemResultado> resultadoFechaList;


    public ResultadoFechaViewAdapter(Context context, ArrayList<ItemResultado> objects) {
        this.mContext = context;
        this.resultadoFechaList = objects;
    }


    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.resultado_fecha_row_item, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final ItemResultado item = resultadoFechaList.get(position);

        viewHolder.tvPosicion.setText((item.getPosicion()));
        viewHolder.tvUsuario.setText((item.getUsuario()));
        viewHolder.tvPuntos.setText((item.getPuntos()));

        // mItemManger is member in RecyclerSwipeAdapter Class
        //mItemManger.bindView(viewHolder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return resultadoFechaList.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    //  ViewHolder Class

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        TextView tvPosicion;
        TextView tvUsuario;
        TextView tvPuntos;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            tvPosicion = (TextView) itemView.findViewById(R.id.tv_posicion);
            tvUsuario = (TextView) itemView.findViewById(R.id.tv_usuario);
            tvPuntos = (TextView) itemView.findViewById(R.id.tv_puntos);

        }
    }

}
