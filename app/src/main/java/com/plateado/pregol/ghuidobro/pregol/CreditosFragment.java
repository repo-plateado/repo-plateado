package com.plateado.pregol.ghuidobro.pregol;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.github.ybq.android.spinkit.style.DoubleBounce;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ghuidobro on 5/22/17.
 */

public class CreditosFragment extends Fragment implements View.OnClickListener{

    private SessionManager session;
    private String usuario;
    private TextView datoPesosDepositados;
    List <MGCredito>mglistpagoCreditoUsuario;
    private LinearLayout creditoDepositadoLL;
    private AlertDialogManager alert = new AlertDialogManager();
    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View fragmentView = inflater.inflate(R.layout.pago_creditos_view, container, false);

        datoPesosDepositados = (TextView) fragmentView.findViewById(R.id.dato_creditos_comprados);
        creditoDepositadoLL = (LinearLayout) fragmentView.findViewById(R.id.credito_depositado_LL);

        progressBar = (ProgressBar)fragmentView.findViewById(R.id.spin_kit);
        DoubleBounce doubleBounce = new DoubleBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);

        mglistpagoCreditoUsuario = new ArrayList();
        session = new SessionManager(getActivity());
        if(session.getUserDetails() != null){
            usuario = session.getUserDetails().get("email");
        }
        if(Utils.isOnline(getActivity())) {
            loadPagoRequest();
        }else{
            alert.showAlertDialog(getActivity(),"Sin Coneccion","Asegurate de tener coneccion a internet.",false);
        }
        return fragmentView;
    }

    private void loadPagoRequest() {
        // load initial data
            try{
                class CreditosTask extends AsyncTask<Void,Void,String> {

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        try {
                            JSONObject jsonObj = new JSONObject(s);
                            convertPagoCreditoJSONtoArrayList(jsonObj);
                            if(!mglistpagoCreditoUsuario.isEmpty()){
                                for (MGCredito item: mglistpagoCreditoUsuario){
                                    datoPesosDepositados.setText(String.valueOf(item.getDepositoUsuario()));
                                }
                            }

                            progressBar.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    protected String doInBackground(Void... v) {
                        RequestHandler rh = new RequestHandler();
                        String res = rh.sendGetRequestParam(Config.URL_PAGO_CREDITO_USUARIO,usuario);
                        return res;
                    }
                }
                CreditosTask creditosTask = new CreditosTask();
                creditosTask.execute();
            }catch(Exception e){
                e.printStackTrace();
            }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void convertPagoCreditoJSONtoArrayList(JSONObject jsonObj) {

        try{
            JSONArray list = jsonObj.getJSONArray("consultapagocreditousuario");

            for(int i=0; i<list.length(); i++) {
                JSONObject stateobj = list.getJSONObject(i);
                mglistpagoCreditoUsuario.add(new MGCredito(stateobj.getString("usuario"),stateobj.getInt("deposito_usuario"),stateobj.getInt("deposito_app"),stateobj.getInt("saldo")));
            }//end of for loop

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }
}