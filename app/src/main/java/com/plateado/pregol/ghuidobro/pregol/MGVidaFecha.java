package com.plateado.pregol.ghuidobro.pregol;

/**
 * Created by ghuidobro on 10/4/17.
 */

public class MGVidaFecha {

    public MGVidaFecha(String vidaFecha, String fechaFixture) {
        this.vidaFecha = vidaFecha;
        this.fechaFixture = fechaFixture;
    }

    public String getVidaFecha() {
        return vidaFecha;
    }

    public void setVidaFecha(String vidaFecha) {
        this.vidaFecha = vidaFecha;
    }

    public String getFechaFixture() {
        return fechaFixture;
    }

    public void setFechaFixture(String fechaFixture) {
        this.fechaFixture = fechaFixture;
    }

    private String vidaFecha;
    private String fechaFixture;
}
