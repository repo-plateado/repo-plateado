package com.plateado.pregol.ghuidobro.pregol;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.util.HashMap;

/**
 * Created by ghuidobro on 5/18/17.
 */

public class UtilsDialogFragment extends DialogFragment {

    private Button buttonAction,buttonCancelar;
    private Dialog d;
    private HashMap<String, String> inscripcionUsuario = new HashMap<>();
    private SessionManager session;
    private String usuario,mainText,secondText,pagoResponse,url;
    private String res = "";
    private RequestHandler rh = new RequestHandler();
    private TextView tvMain,tvSecond;
    private Boolean esInscripcion;
    private final int layout;
    private Fragment fragment;

    public UtilsDialogFragment(String pagoResponse,int layout,Fragment fragment, Boolean esInscripcion, String url) {
        this.layout = layout;
        this.fragment = fragment;
        this.pagoResponse = pagoResponse;
        this.url = url;
        this.esInscripcion = esInscripcion;
    }


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        d = new Dialog(getActivity());
        session = new SessionManager(getActivity());

        if(session.getUserDetails() != null){
            usuario = session.getUserDetails().get("email");
        }

        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(layout);

        buttonAction = (Button) d.findViewById(R.id.buttonAction);
        buttonCancelar = (Button) d.findViewById(R.id.buttonCancelar);
        tvMain = d.findViewById(R.id.tv_dialog_main);
        tvSecond = d.findViewById(R.id.tv_dialog_second);

        if(esInscripcion){
            mainText = getResources().getString(R.string.mainTextInscripcion);
            secondText = getResources().getString(R.string.secondTextInscripcion);
        }else {
            mainText = getResources().getString(R.string.mainTextDelete);
            secondText = getResources().getString(R.string.secondTextDelete);
        }

        tvMain.setText(mainText);
        tvSecond.setText(secondText);

        buttonAction.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                buttonAction.setEnabled(false);
                asincronaGenericaDialogFragment(pagoResponse,usuario, fragment,url,esInscripcion);
            }
        });
        buttonCancelar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });

        d.show();

        return d;
    }

    private void asincronaGenericaDialogFragment(final String pagoResponse, final String usuario, final Fragment fragment, final String url, final Boolean esPago) {
        // load initial data

        try{
            class CreditosTask extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    d.dismiss();
                    replaceFragment(fragment);
                }

                @Override
                protected String doInBackground(Void... v) {
                    if(esPago){
                        inscripcionUsuario.put("registroPago",pagoResponse);
                        inscripcionUsuario.put("usuario",usuario);
                        res = rh.sendPostRequest(url,inscripcionUsuario);
                    }else{
                        res = rh.sendGetRequestParam(url,usuario);
                    }

                    return res;
                }
            }
            CreditosTask creditosTask = new CreditosTask();
            creditosTask.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }

}