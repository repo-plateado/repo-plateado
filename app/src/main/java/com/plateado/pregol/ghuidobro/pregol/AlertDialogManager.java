package com.plateado.pregol.ghuidobro.pregol;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by ghuidobro on 5/31/17.
 */

public class AlertDialogManager {
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        if (status != null)
            // Setting alert dialog icon
            builder.setIcon((status) ? R.drawable.app_logo86 : R.drawable.icon_info_bet);

        builder.setMessage(message);
        builder.setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();

    }
}
