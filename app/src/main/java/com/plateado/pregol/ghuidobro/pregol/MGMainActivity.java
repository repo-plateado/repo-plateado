package com.plateado.pregol.ghuidobro.pregol;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.navigation.NavigationView;

import java.util.HashMap;

public class MGMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Session Manager Class
    private SessionManager session;
    private String usuario;
    HashMap<String, String> creditousuario = new HashMap<>(); ;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_activity_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        session = new SessionManager(getApplicationContext());

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //add this line to display menu1 when the activity is loaded
        displaySelectedScreen(R.id.nav_inicio);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void displaySelectedScreen(int itemId) {
        Fragment fragment = null;
        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_inicio:
                fragment = new AuthFragment();
                break;
            case R.id.nav_jugar:
                if(session.isLoggedIn()){
                fragment = new MGMainFixtureFragment();
                }else{
                    fragment = new AuthFragment();
                }

                break;
            case R.id.nav_predicciones:
                if(session.isLoggedIn()){
                    fragment = new MGPrediccionesFragment();
                }else{
                    fragment = new AuthFragment();
                }
                break;
                /*
            case R.id.nav_creditos:
                if(session.isLoggedIn()){
                    fragment = new CreditosFragment();
                }else{
                    fragment = new AuthFragment();
                }
                break;
                 */
            case R.id.nav_estadistica:
                fragment = new EstadisticasFragment();
                break;

            case R.id.nav_reglas:
                fragment = new ReglasFragment();
                break;
        }

        replaceFragment(fragment);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void replaceFragment(Fragment fragment) {
        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        //calling the method displayselectedscreen and passing the id of selected menu
        displaySelectedScreen(item.getItemId());
        //make this method blank
        return true;
    }

    // Espera los resultados del checkout
    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent data) {

        super.onActivityResult(requestCode,resultCode, data);

    }

    private void savePagoResponse(final String pagoResponse, final String usuario) {
        // load initial data
        try{
            class CreditosTask extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    Fragment fragment = new CreditosFragment();
                    replaceFragment(fragment);
                }

                @Override
                protected String doInBackground(Void... v) {
                    creditousuario.put("pago",pagoResponse);
                    creditousuario.put("usuario",usuario);
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendPostRequest(Config.URL_PAGO_CREDITO,creditousuario);
                    return res;
                }
            }
            CreditosTask creditosTask = new CreditosTask();
            creditosTask.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }
    }


