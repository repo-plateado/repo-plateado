package com.plateado.pregol.ghuidobro.pregol;

/**
 * Created by ghuidobro on 5/15/17.
 */

public class ItemResultado {

    private static final long serialVersionUID = 1L;

    private String posicion;
    private String usuario;
    private String puntos;
    private String fechaFixture;

    public ItemResultado(String posicion, String usuario, String puntaje,String fechaFixture) {
        this.posicion = posicion;
        this.usuario = usuario;
        this.puntos = puntaje;
        this.fechaFixture = fechaFixture;
    }


    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPuntos() {
        return puntos;
    }

    public void setPuntos(String puntos) {
        this.puntos = puntos;
    }
    public String getFechaFixture() {
        return fechaFixture;
    }

    public void setFechaFixture(String fechaFixture) {
        this.fechaFixture = fechaFixture;
    }

}
