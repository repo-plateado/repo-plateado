package com.plateado.pregol.ghuidobro.pregol;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

@SuppressLint("ValidFragment")
public class MGDialogClass<deleteInscripcionPrediccion> extends DialogFragment {


    private final int layout;
    private Dialog d;
    private Button buttonCancelar;
    private Boolean esFixture;
    private TextView tvMain;
    private String mainText;

    @SuppressLint("ValidFragment")
    public MGDialogClass(int layout,Boolean esFixture) {
        this.layout = layout;
        this.esFixture = esFixture;
    }


    public Dialog onCreateDialog(Bundle savedInstanceState) {

        d = new Dialog(getActivity());
        d.setContentView(layout);
        buttonCancelar = d.findViewById(R.id.buttonCancelar);
        tvMain = d.findViewById(R.id.tv_info_main_text);

        if(esFixture){
            mainText = getResources().getString(R.string.info_message_fixture);
        }else {
            mainText = getResources().getString(R.string.info_message);
        }

        tvMain.setText(mainText);

        buttonCancelar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });

        d.show();

        return d;

        }

    }

