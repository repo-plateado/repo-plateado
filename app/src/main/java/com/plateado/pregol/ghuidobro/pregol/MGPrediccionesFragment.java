package com.plateado.pregol.ghuidobro.pregol;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.util.Attributes;
import com.github.fafaldo.fabtoolbar.widget.FABToolbarLayout;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ghuidobro on 4/12/17.
 */
public class MGPrediccionesFragment extends Fragment {

    private ArrayList<MGFixture> mDataSet;
    private RecyclerView mRecyclerView;
    private PrediccionUsuarioViewAdapter mAdapter;
    private ArrayList<MGItemPrediction> MGItemPredictions = new ArrayList<>();
    private FABToolbarLayout morph;
    private FloatingActionButton fab;
    private Handler handler;
    private View fragmentView;
    private View infoIcon, borrarJugada, sendPayIcon, cuatro;
    private boolean isPrediction = false;
    private ArrayList<MGPrediccionUsuario> mDataSetPrediccion;
    List<MGPagoInscripcion> mglistRegistroInscripcionUsuario;
    private boolean isPredictionDone = false;
    private boolean isPredictionempty = false;
    private TextView labelInscripcion,fechaFixture;
    private SessionManager session;
    private String usuario;
    private Utils util = new Utils();
    AlertDialogManager alert = new AlertDialogManager();
    private ImageView iconApp,appLogo;
    private TextView emptyView;
    private List <MGCredito>mglistpagoCreditoUsuario;
    private List <MGVidaFecha>mglistVidaFecha;
    private boolean fondosDisponibles = true;
    private LinearLayout cabeceraPrediccion;
    private ProgressBar progressBar;
    private Boolean vidaFechaBool;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceStat) {
        fragmentView = inflater.inflate(R.layout.pagorecyclerview, container, false);

        emptyView = (TextView) fragmentView.findViewById(R.id.empty_view);
        iconApp = (ImageView) fragmentView.findViewById(R.id.iconApp);
        appLogo = (ImageView) fragmentView.findViewById(R.id.app_logo);
        mRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.my_recycler_view);
        morph = (FABToolbarLayout) fragmentView.findViewById(R.id.fabtoolbar);
        sendPayIcon = fragmentView.findViewById(R.id.send_pay_icon);
        fab = (FloatingActionButton) fragmentView.findViewById(R.id.fab);
        infoIcon = fragmentView.findViewById(R.id.info_icon);
        borrarJugada = fragmentView.findViewById(R.id.borrar_jugada);
        cuatro = fragmentView.findViewById(R.id.btn_main_action);
        labelInscripcion = (TextView) fragmentView.findViewById(R.id.label_inscripcion);
        fechaFixture = (TextView) fragmentView.findViewById(R.id.fecha_fixture);
        cabeceraPrediccion = (LinearLayout) fragmentView.findViewById(R.id.cabecera_prediccion_ll);
        // Layout Managers:
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        progressBar = (ProgressBar)fragmentView.findViewById(R.id.spin_kit);
        DoubleBounce doubleBounce = new DoubleBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);

        session = new SessionManager(getActivity());
        if(session.getUserDetails() != null){
            usuario = session.getUserDetails().get("email");
        }
        mDataSet = new ArrayList<MGFixture>();
        mDataSetPrediccion = new ArrayList<MGPrediccionUsuario>();
        mglistRegistroInscripcionUsuario = new ArrayList<MGPagoInscripcion>();
        handler = new Handler();
        mglistpagoCreditoUsuario = new ArrayList();
        mglistVidaFecha = new ArrayList();

        if(Utils.isOnline(getActivity())) {
            loadVidaFechaRequest();
            loadPrediccionData();
            loadData();
            loadPagoInscripcionRequest();
            loadPagoRequest();
        }else{
            alert.showAlertDialog(getActivity(),"Sin Coneccion","Asegurate de tener coneccion a internet.",false);
        }

        mAdapter = new PrediccionUsuarioViewAdapter(getActivity(), MGItemPredictions);

        mAdapter.setMode(Attributes.Mode.Single);

        mRecyclerView.setAdapter(mAdapter);

        /* Scroll Listeners */
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.i("RecyclerView", "onScrollStateChanged");
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.fab) {
                    morph.show();
                }
                morph.hide();
            }
        });
        infoIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callInfoDialog();
                morph.hide();

            }
        });

        borrarJugada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                morph.hide();
                Fragment fragment = new MGMainFixtureFragment();
                UtilsDialogFragment newFragment = new UtilsDialogFragment(null,R.layout.utils_custom_dialog,fragment,false,Config.URL_BORRADO_INSCRIPCION_PREDICCION_USUARIO);
                newFragment.show(getFragmentManager(),"");
            }
        });

        sendPayIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    callPagoDialog();
                morph.hide();
            }
        });

        cuatro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                morph.hide();
            }
        });

        return fragmentView;
    }

    public void callInfoDialog() {
        DialogFragment newFragment = new MGDialogClass(R.layout.info_custom_dialog,false);
        newFragment.show(getActivity().getFragmentManager(),"");

    }

    public void callPagoDialog() {
        if(fondosDisponibles){
            Fragment fragment = new MGMainFixtureFragment();
            UtilsDialogFragment newFragment = new UtilsDialogFragment(Config.KEY_INSCRIPCION_FECHA,R.layout.utils_custom_dialog,fragment,true,Config.URL_REGISTRO_PAGO_USUARIO);
            newFragment.show(getFragmentManager(),"");
        }else{
            AlertDialogManager alert = new AlertDialogManager();
            alert.showAlertDialog(getActivity(),"Pisala un poco", "No tenes fondos para participar, en creditos te decimos como hacerlo.",true);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    // load initial data
    public void loadData() {
        String urlString = Config.URL_FIXTURE;

        try{
            URL url = new URL(urlString);
            StateTask stateTask = new StateTask();
            stateTask.execute(url);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    // load initial data
    public void loadPrediccionData() {
        final String urlString = Config.URL_PREDICCION_USUARIO;
        try{
            class AddFixture extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    new Thread(new Task()).start();
                    progressBar.setVisibility(View.VISIBLE);

                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        convertPredictionJSONtoArrayList(jsonObj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendGetRequestParam(urlString, usuario);
                    return res;
                }
            }
            AddFixture addFixture = new AddFixture();
            addFixture.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private class StateTask extends AsyncTask<URL, String,JSONObject> {

        @Override
        protected JSONObject doInBackground(URL... params) {

            HttpURLConnection connection = null;
            try{

                connection = (HttpURLConnection)params[0].openConnection();
                int response = connection.getResponseCode();

                if(response == HttpURLConnection.HTTP_OK){

                    StringBuilder builder = new StringBuilder();
                    try {
                        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String line;
                        while ((line = reader.readLine())!= null ) {
                            builder.append(line);
                        }
                    }catch (IOException e){
                        publishProgress("Read Error");
                        e.printStackTrace();
                    }//inner try

                    return new JSONObject(builder.toString());

                }else{
                    publishProgress("Connection Error");
                }


            }//end outer try
            catch (Exception e){

                publishProgress("Connection Error 2");
                e.printStackTrace();

            }finally {
                connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
        }//end of onProgressUpdate

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if(isPrediction && !isPredictionempty && isPredictionDone) {
                convertFixtureJSONtoArrayList(jsonObject);
                setItemFixture();
            }else if(!isPrediction && !isPredictionempty && !isPredictionDone){

                mRecyclerView.setVisibility(View.GONE);
                morph.setVisibility(View.GONE);
                emptyView.setText(R.string.label_espera_datos);
                emptyView.setVisibility(View.VISIBLE);
                iconApp.setVisibility(View.VISIBLE);
                appLogo.setVisibility(View.VISIBLE);

                convertPredictionJSONtoArrayList(jsonObject);
            }else if(isPrediction && isPredictionempty){
                mRecyclerView.setVisibility(View.GONE);
                morph.setVisibility(View.GONE);
                emptyView.setText(R.string.estado_prediccion_message);
                emptyView.setVisibility(View.VISIBLE);
                iconApp.setVisibility(View.VISIBLE);
                appLogo.setVisibility(View.VISIBLE);
            }

        }//end of onPostExecute
    }//end of

    private void setItemFixture() {

        for (MGFixture item:mDataSet) {
            MGItemPrediction MGItemPrediction = new MGItemPrediction();
            MGItemPrediction.setMGFixture(item);

            for(MGPrediccionUsuario prediccion : mDataSetPrediccion){
                if(item.getIdPartido().equals(prediccion.getIdPartido())){
                    MGItemPrediction.setEstado(Integer.parseInt(prediccion.getResultadoPrediccion()));
                }
            }

            MGItemPredictions.add(MGItemPrediction);
        }
        mAdapter.notifyDataSetChanged();

            mRecyclerView.setVisibility(View.VISIBLE);
            morph.setVisibility(View.VISIBLE);
            cabeceraPrediccion.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            iconApp.setVisibility(View.GONE);
            appLogo.setVisibility(View.GONE);

    }


    private void convertFixtureJSONtoArrayList(JSONObject states){

        mDataSet.clear();

        try{
            JSONArray list = states.getJSONArray("fixture");

            for(int i=0; i<list.length(); i++) {
                JSONObject stateobj = list.getJSONObject(i);
                mDataSet.add(new MGFixture(stateobj.getString("equipo_local"),stateobj.getString("equipo_visitante"),stateobj.getString("escudo_local"),stateobj.getString("escudo_visita"),stateobj.getString("id_partido"),stateobj.getString("numero_fecha_afa")));
            }//end of for loop
        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }//end of convertJSONArrayList

    private void convertPredictionJSONtoArrayList(JSONObject states){
            isPrediction = true;
        try{

            JSONArray list = states.getJSONArray("prediccionusuario");
            if(list.length()>0) {
                for (int i = 0; i < list.length(); i++) {
                    JSONObject stateobj = list.getJSONObject(i);
                    mDataSetPrediccion.add(new MGPrediccionUsuario(stateobj.getString("fecha_fixture"), stateobj.getString("id_partido"), stateobj.getString("resultado"), stateobj.getString("usuario")));
                }//end of for loop
                isPredictionDone = true;
            }else{
                isPredictionempty = true;
            }


        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }//end of convertJSONArrayList

    private class Task implements Runnable {
        @Override
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            handler.post(new Runnable() {
                @Override
                public void run() {
                    //loading = ProgressDialog.show(getActivity(), "Enviando...", "Un momento...", false, false);
                }
            });
        }
    }

    private void loadPagoInscripcionRequest() {

        try{
            class CreditosTask extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    String registroPago = null;
                    super.onPostExecute(s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        convertConsultaInscripcionJSONtoArrayList(jsonObj);
                        if(!mglistRegistroInscripcionUsuario.isEmpty()){

                            for (MGPagoInscripcion item: mglistRegistroInscripcionUsuario){
                                if(item.getFechaFixture() != null || !item.getFechaFixture().equals("")){
                                    registroPago = "Participando";
                                }
                                fechaFixture.setText("Fecha:  "+ item.getFechaFixture());
                                sendPayIcon.setVisibility(View.GONE);
                            }
                        }else{
                            registroPago = "No Inscripto";
                            sendPayIcon.setVisibility(View.VISIBLE);
                        }
                        labelInscripcion.setText(registroPago);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendGetRequestParam(Config.URL_REGISTRO_PAGO_USUARIO_CONSULTA,usuario);
                    return res;
                }
            }
            CreditosTask creditosTask = new CreditosTask();
            creditosTask.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void convertConsultaInscripcionJSONtoArrayList(JSONObject jsonObj) {

        try {
            JSONArray list = jsonObj.getJSONArray("consultapagoinscripcionusuario");

            if (list.length() == 0 && !isPredictionempty) {
                if(!vidaFechaBool){
                    alert.showAlertDialog(getActivity(),"Lo Sentimos!", "No hay alargue, se deshabilito la fecha, empiezan los partidos.",true);

                    //working

                }else{
                    alert.showAlertDialog(getActivity(), "No hay inscripcion", "Hiciste una inteligente jugada, pero no estas participando, tienes que apurarte a inscribirte.", false);
                }
            }else{
                for(int i=0; i<list.length(); i++) {
                    JSONObject stateobj = list.getJSONObject(i);
                    mglistRegistroInscripcionUsuario.add(new MGPagoInscripcion(stateobj.getString("usuario"),stateobj.getString("fecha_fixture"),stateobj.getString("registro_pago")));
                }//end of for loop
            }

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }

    private void loadPagoRequest() {
        // load initial data
        try{
            class CreditosTask extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        convertPagoCreditoJSONtoArrayList(jsonObj);
                        if(mglistpagoCreditoUsuario.isEmpty()){
                            fondosDisponibles = false;
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendGetRequestParam(Config.URL_PAGO_CREDITO_USUARIO,usuario);
                    return res;
                }
            }
            CreditosTask creditosTask = new CreditosTask();
            creditosTask.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void convertPagoCreditoJSONtoArrayList(JSONObject jsonObj) {

        try{
            JSONArray list = jsonObj.getJSONArray("consultapagocreditousuario");

            for(int i=0; i<list.length(); i++) {
                JSONObject stateobj = list.getJSONObject(i);
                mglistpagoCreditoUsuario.add(new MGCredito(stateobj.getString("usuario"),stateobj.getInt("deposito_usuario"),stateobj.getInt("deposito_app"),stateobj.getInt("saldo")));
            }//end of for loop

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }


    private void loadVidaFechaRequest() {
        // load initial data
        try{
            class CreditosTask extends AsyncTask<Void,Void,String> {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected void onPostExecute(String s) {
                    super.onPostExecute(s);
                    try {
                        JSONObject jsonObj = new JSONObject(s);
                        convertVidaFechaJSONtoArrayList(jsonObj);
                        if(!mglistVidaFecha.isEmpty()){
                            for (MGVidaFecha item: mglistVidaFecha){
                                if(item.getVidaFecha() != null && item.getVidaFecha().equals("False")){
                                    vidaFechaBool = false;
                                    sendPayIcon.setVisibility(View.GONE);
                                    borrarJugada.setVisibility(View.GONE);
                                }else{
                                    vidaFechaBool = true;
                                    sendPayIcon.setVisibility(View.VISIBLE);
                                    borrarJugada.setVisibility(View.VISIBLE);
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                protected String doInBackground(Void... v) {
                    RequestHandler rh = new RequestHandler();
                    String res = rh.sendGetRequest(Config.URL_CONSULTA_VIDA_FECHA);
                    return res;
                }
            }
            CreditosTask creditosTask = new CreditosTask();
            creditosTask.execute();
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void convertVidaFechaJSONtoArrayList(JSONObject jsonObj) {

        try{
            JSONArray list = jsonObj.getJSONArray("vidafecha");

            for(int i=0; i<list.length(); i++) {
                JSONObject stateobj = list.getJSONObject(i);
                mglistVidaFecha.add(new MGVidaFecha(stateobj.getString("vida"),stateobj.getString("fecha_fixture")));
            }//end of for loop

        }catch(JSONException e){
            e.printStackTrace();

        }//end of catch

    }

    private void replaceFragment(Fragment fragment) {
        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }

}
